// ignore_for_file: prefer_const_constructors

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: HomePage(),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  TextEditingController nameC = TextEditingController();
  TextEditingController jobC = TextEditingController();
  String hasilname = 'data kosong';
  String hasiljob = 'data kosong';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('PUT/PATCH'),
      ),
      body: ListView(
        children: [
          Column(
            children: [
              // ignore: prefer_const_constructors, duplicate_ignore
              Padding(
                padding: EdgeInsets.all(8.0),
                // ignore: prefer_const_constructors
                child: TextField(
                  controller: nameC,
                  autocorrect: false,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Colors.black)),
                      label: Text('name'),
                      labelStyle: TextStyle(color: Colors.black)),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8.0),
                child: TextField(
                  controller: jobC,
                  autocorrect: false,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.black)),
                    border: OutlineInputBorder(),
                    labelStyle: TextStyle(color: Colors.black),
                    label: Text('job'),
                  ),
                ),
              ),

              ElevatedButton(
                  onPressed: () async {
                    var myHtpp = await http.put(
                        Uri.parse('https://reqres.in/api/users/2'),
                        body: {'name': nameC.text, 'job': jobC.text});
                    setState(() {
                      Map<String, dynamic> data =
                          json.decode(myHtpp.body) as Map<String, dynamic>;
                      hasilname = '${data['name']}';
                      hasiljob = '${data['job']}';
                    });
                  },
                  child: Text('submit')),
              Text(hasilname),
              Text(hasiljob),
            ],
          )
        ],
      ),
    );
  }
}
